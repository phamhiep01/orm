﻿// khởi tạo đối tượng context đại diện cho 1 database
var context = new AppDbContext();
var names = context.Persons.Skip(1).Take(1);
foreach (var name in names)
{
    Console.WriteLine(name.Name);
}

Console.WriteLine( "done");

// làm tương tự với những từ khóa LinQ đã học và bật SQL profiler lên xem câu truy vấn được generate như thế nào
//orderby
//groupby
//aggregate function sum, avg, count, max, min
//first, firstOrDefault, last, LastOrDefault, Single, SingleOrDefault, paging,
