﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;

var context = new AppDbContext();

// tạo danh sách Product sẽ thêm vào database
var s = new StreamReader("MOCK_DATA.json");
var jr = new JsonTextReader(s);
var js = new JsonSerializer();
var obj = js.Deserialize<List<Product>>(jr);
// đánh dấu là thêm vào database
context.Products.AddRange(obj);

// lưu mọi sự thay đổi
context.SaveChanges();
var number = context.Products.Count();
Console.WriteLine($"number of record is: {number}");

////Lấy danh sách tất cả các sản phẩm.
//var allProducts = context.Products.ToList();

////Lấy danh sách tên của tất cả các sản phẩm.
//var allProductNames = context.Products.Select(p => p.Name).ToList();

////Lấy danh sách các sản phẩm có giá lớn hơn 50 đồng.
//var productsWithPriceGreaterThan50 = context.Products.Where(p => p.Price > 50).ToList();

////Lấy danh sách các sản phẩm có số lượng tồn kho ít hơn 10.
//var productsWithStockLessThan10 = context.Products.Where(p => p.StockQuantity < 10).ToList();

////Lấy danh sách tên của các sản phẩm có giá từ 100 đến 500 đồng.
//var productsWithPriceBetween100And500 = context.Products.Where(p => p.Price >= 100 && p.Price <= 500).Select(p => p.Name).ToList();

////Lấy danh sách các sản phẩm được tạo ra trong vòng 7 ngày gần đây.
//var sevenDaysAgo = DateTime.Now.AddDays(-7);
//var productsCreatedInLast7Days = context.Products.Where(p => p.CreatedAt >= sevenDaysAgo).ToList();

////Lấy danh sách các sản phẩm không còn tồn tại (không hoạt động).
//var inactiveProducts = context.Products.Where(p =>!p.IsActive).ToList();

////Lấy tổng số lượng tồn kho của tất cả sản phẩm.
//var totalStock = context.Products.Sum(p => p.StockQuantity);

////Lấy tên của sản phẩm và ngày tạo của sản phẩm có giá lớn hơn 200 đồng.
//var productsWithPriceGreaterThan200 = context.Products.Where(p => p.Price > 200).Select(p => new { p.Name, p.CreatedAt }).ToList();

////Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
//var productsSortedByPriceDescending = context.Products.OrderByDescending(p => p.Price).ToList();


////Order By:

////Lấy danh sách các sản phẩm được sắp xếp theo tên sản phẩm tăng dần.
//var productsSortedByNameAscending = context.Products.OrderBy(p => p.Name).ToList();

////Lấy danh sách các sản phẩm được sắp xếp theo giá giảm dần.
//var productsSortedByPriceDescending2 = context.Products.OrderByDescending(p => p.Price).ToList();

////Lấy danh sách các sản phẩm được sắp xếp theo ngày tạo mới nhất đến cũ nhất.
//var productsSortedByCreatedAtDescending = context.Products.OrderByDescending(p => p.CreatedAt).ToList();


////Group By:

////Lấy danh sách các sản phẩm được nhóm theo số lượng tồn kho. (Mỗi nhóm hiển thị số lượng và tên các sản phẩm trong nhóm đó)
//var productsGroupedByStock = context.Products.GroupBy(p => p.StockQuantity).Select(g => new { StockCount = g.Key, ProductNames = g.Select(p => p.Name) }).ToList();

////Lấy danh sách các sản phẩm được nhóm theo khoảng giá (0-100, 101-200, v.v.). (Mỗi nhóm hiển thị khoảng giá và số lượng sản phẩm trong nhóm đó)
//var productsGroupedByPriceRange = context.Products.GroupBy(p => (int)(p.Price / 100)).Select(g => new { PriceRange = $"{g.Key * 100}-{(g.Key + 1) * 100 - 1}", ProductCount = g.Count() }).ToList();


////First, FirstOrDefault, Last, LastOrDefault, Single, SingleOrDefault:

////Lấy thông tin chi tiết của sản phẩm đầu tiên trong danh sách.
//var firstProductDetails = context.Products.First();

////Lấy thông tin chi tiết của sản phẩm có giá cuối cùng.
//var lastProductDetailsByPrice = context.Products.OrderByDescending(p => p.Price).First();

////Lấy thông tin chi tiết của sản phẩm có tên là "iPhone" (nếu có).
//var iPhoneDetails = context.Products.FirstOrDefault(p => p.Name == "iPhone");

////Lấy thông tin chi tiết của sản phẩm có ID là 5 (nếu có).
//var productWithId5Details = context.Products.FirstOrDefault(p => p.Id == 5);

////Lấy thông tin chi tiết của sản phẩm có giá là 100 (nếu có).
//var productWithPrice100Details = context.Products.FirstOrDefault(p => p.Price == 100);


////Paging:

////Lấy danh sách 10 sản phẩm đầu tiên trong danh sách.
//var first10Products = context.Products.Take(10).ToList();

////Lấy danh sách 20 sản phẩm từ vị trí thứ 21 trong danh sách.
//var next20Products = context.Products.Skip(20).Take(20).ToList();

var frist = context.Products.FirstOrDefault( p => p.Name == "H");
Console.WriteLine(frist);

context.SaveChanges();
