﻿using Microsoft.EntityFrameworkCore;

//NoTracking
var context = new AppDbContext();
var person =context.Persons.AsNoTracking().First();
context.Entry(person).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
var count = context.SaveChanges();
Console.WriteLine(count);


