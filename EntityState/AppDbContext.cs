﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = "Data Source=DESKTOP-9FP7ODG;Database=EntityState;Trusted_Connection=True;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connectionString);
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Person>().HasData(
        new Person { Id = 1, Name = "Jane Austen" },
        new Person { Id = 2, Name = "Charles Dickens" },
        new Person { Id = 3, Name = "Mark Twain" }
        );
    }
}
